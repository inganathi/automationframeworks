﻿

using AutomationProject.Base;
using SeleniumExtras.PageObjects;

namespace AutomationProject.Base
{

    public abstract class BasePage : Base
    {
        public BasePage()
        {
            PageFactory.InitElements(DriverContext.Driver, this);
        }

    }
}